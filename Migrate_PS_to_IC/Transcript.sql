-- ----------------------------------------------------------------------------
-- Author: Tanner Crook
-- Copyright: (c) Tanner Crook 2017
-- ----------------------------------------------------------------------------
-- Gets transcript data from PowerSchool for conversion to Infinite Campus.
-- ----------------------------------------------------------------------------





SELECT DISTINCT
       s.student_number AS "studentNum"
     , sg.course_number AS "courseNum"
     , c.course_name    AS "courseName"
     , (SELECT DISTINCT EXTRACT(year FROM t2.lastDay) FROM terms t2 WHERE t2.yearID = t.yearID AND t2.IsYearRec = 1) AS "endYear"
     , sg.grade_level AS "gradeLevel"
     , sg.grade_level AS "ncesGrade"
     , NULL AS "repeatCourse"
     , 1202000 AS "districtNum"
     , sg.schoolID AS "schoolNum"
     , sch.name AS "schoolName"
     , ps_customfields.getCoursescf(c.id,'alt_course_number') AS "stateCode"
     , DECODE(cwy.scholarshipCourse 
               , 'Y', 'Y'
               , 'N', 'N'
               , NULL) AS "hathaway"
     , sg.grade AS "score"
     , NULL AS "onlineLearning"
     , NULL AS "gpaWeight"
     , sg.gpa_points AS "weightedGPAValue"
     , sg.gpa_points AS "unweightedGPAValue"
     , 4.0 AS "gpaMax"
     , NULL AS "bonusPoints"
     , sg.earnedCrHrs AS "creditsEarned"
     , sg.potentialCrHrs AS "creditsAttempted"
     , DECODE(sg.schoolID
               , 1202056, 'High School'
               , 1202055, 'High School'
               , 1202057, 'High School'
               , 1202051, 'Middle School'
               , 1202001, 'Elementary'
               , 1202002, 'Elementary'
               , 1202003, 'Elementary'
               , 1202004, 'Elementary'
               , 1202005, 'Elementary'
               , NULL) AS "creditGroupName"
     , CASE WHEN sg.schoolID < 1202050 THEN 'Elementary Courses'
          ELSE DECODE(sg.credit_type
               , 'CAR/VOC', 'Career/Vocational'
               , 'Car/Voc', 'Career/Vocational'
               , 'ENG', 'English'
               , 'MATH', 'Math'
               , 'ELECT', 'Elective'
               , 'PE', 'Physical Education'
               , 'HEALTH', 'Health'
               , 'SCI', 'Science'
               , 'SOC', 'Social Studies'
               , 'FINE', 'Fine Arts'
               , 'FL', 'Foreign Language'
               , NULL, NULL
               , '#BAD') END AS "creditTypeName"
     , NULL AS "creditTypeNameOverride"
     , TO_CHAR(sg.dateStored, 'mm/dd/yyyy') AS "date"
     , CASE sg.storeCode
               WHEN 'S1' THEN 1
               WHEN 'S2' THEN 2
               WHEN 'T3' THEN 1
               END AS "actualTerm"
     , CASE WHEN t.isYearRec = 1 THEN 1
          ELSE DECODE(sg.storeCode
               , 'S1', 1
               , 'S2', 2
               , 'T3', 1) END AS "startTerm"
     , CASE WHEN t.isYearRec = 1 AND sg.schoolID < 1202050 THEN 1
          WHEN t.isYearRec = 1 AND sg.schoolID > 1202051 THEN 2
          ELSE DECODE(sg.storeCode
               , 'S1', 1
               , 'S2', 2
               , 'T3', 1) END AS "endTerm"
     , CASE WHEN t.isYearRec = 1 AND sg.schoolID < 1202050 THEN 1
          WHEN t.isYearRec = 1 AND sg.schoolID > 1202051 THEN 2
          ELSE 1 END AS "termsLong"
     , CASE WHEN sg.schoolID < 1202050 THEN 1
          ELSE 2 END AS calendarTerms
     , TO_CHAR(t.firstDay, 'mm/dd/yyyy') AS "termStartDate"
     ,  TO_CHAR(t.lastDay, 'mm/dd/yyyy') AS "termEndDate"
     , NULL AS "comments"
     , NULL AS "additionalKey"
FROM students s INNER JOIN storedGrades sg
ON s.ID = sg.studentID
INNER JOIN courses c
ON sg.course_number = c.course_number
INNER JOIN terms t
ON sg.termID = t.ID
AND sg.schoolID = t.schoolID
INNER JOIN schools sch
ON sg.schoolID = sch.school_number
INNER JOIN sections sec
ON sg.sectionID = sec.ID
INNER JOIN U_DEF_EXT_Students sue
ON s.DCID = sue.studentsDCID
INNER JOIN s_wy_crs_x cwy
ON c.DCID = cwy.coursesDCID
WHERE sue.ic_personID IS NOT NULL
AND sg.storeCode IN ('S1','S2','T3')
ORDER BY 1, 4, 2;


















-- Transfer credit grades

SELECT DISTINCT
	  s.student_number AS "studentNum"
	, sg.COURSE_NUMBER AS "courseNum"
	, sg.course_name AS "courseName" 
	, (SELECT DISTINCT EXTRACT(year FROM t2.lastDay) FROM terms t2 WHERE t2.yearID = t.yearID AND t2.IsYearRec = 1) AS "endYear"
	, sg.grade_level AS "gradeLevel"
	, sg.grade_level AS "ncesGrade"
	, NULL AS "repeatCourse"
	, 1202000 AS "districtNum"
	, sg.schoolID AS "schoolNum"
	, sg.schoolName AS "schoolName"
	, ps_customfields.getCoursescf(c.id,'alt_course_number') AS "stateCode"
	, NULL AS "hathaway"
	, sg.grade AS "score"
	, NULL AS "onlineLearning"
	, 1.0 AS "gpaWeight"
	, sg.gpa_points AS "weightedGPAValue"
	, sg.gpa_points AS "unweightedGPAValue"
	, 4.0 AS "gpaMax"
	, NULL AS "bonusPoints"
	, sg.earnedCrHrs AS "creditsEarned"
	, sg.potentialCrHrs AS "creditsAttempted"
	, DECODE(sg.schoolID
               , 1202056, 'High School'
               , 1202055, 'High School'
               , 1202057, 'High School'
               , 1202051, 'Middle School'
               , 1202001, 'Elementary'
               , 1202002, 'Elementary'
               , 1202003, 'Elementary'
               , 1202004, 'Elementary'
               , 1202005, 'Elementary'
               , NULL) AS "creditGroupName"
	, CASE WHEN sg.schoolID < 1202050 THEN 'Elementary Courses'
          ELSE DECODE(sg.credit_type
               , 'CAR/VOC', 'Career/Vocational'
               , 'Car/Voc', 'Career/Vocational'
               , 'ENG', 'English'
               , 'MATH', 'Math'
               , 'ELECT', 'Elective'
               , 'PE', 'Physical Education'
               , 'HEALTH', 'Health'
               , 'SCI', 'Science'
               , 'SOC', 'Social Studies'
               , 'FINE', 'Fine Arts'
               , 'FL', 'Foreign Language'
               , NULL, NULL
               , '#BAD') END AS "creditTypeName"
	, NULL AS "creditTypeNameOverride"
	, TO_CHAR(sg.dateStored, 'mm/dd/yyyy') AS "date"
     , CASE sg.storeCode
               WHEN 'S1' THEN 1
               WHEN 'S2' THEN 2
               WHEN 'T3' THEN 1
               END AS "actualTerm"
     , CASE WHEN t.isYearRec = 1 THEN 1
          ELSE DECODE(sg.storeCode
               , 'S1', 1
               , 'S2', 2
               , 'T3', 1) END AS "startTerm"
     , CASE WHEN t.isYearRec = 1 AND sg.schoolID < 1202050 THEN 1
          WHEN t.isYearRec = 1 AND sg.schoolID > 1202051 THEN 2
          ELSE DECODE(sg.storeCode
               , 'S1', 1
               , 'S2', 2
               , 'T3', 1) END AS "endTerm"
     , CASE WHEN t.isYearRec = 1 AND sg.schoolID < 1202050 THEN 1
          WHEN t.isYearRec = 1 AND sg.schoolID > 1202051 THEN 2
          ELSE 1 END AS "termsLong"
     , CASE WHEN sg.schoolID < 1202050 THEN 1
          ELSE 2 END AS calendarTerms
     , TO_CHAR(t.firstDay, 'mm/dd/yyyy') AS "termStartDate"
     ,  TO_CHAR(t.lastDay, 'mm/dd/yyyy') AS "termEndDate"
     , NULL AS "comments"
     , NULL AS "additionalKey"
     , sg.credit_Type
FROM storedgrades sg
INNER JOIN courses c
ON sg.course_number = c.course_number
INNER JOIN students s
ON sg.studentID = s.ID
INNER JOIN U_DEF_EXT_STUDENTS u 
ON s.DCID = u.studentsDCID 
INNER JOIN terms t 
ON sg.termID = t.ID 
AND sg.schoolID = t.schoolID
WHERE sg.course_number LIKE 'TR%'
AND u.IC_PersonID IS NOT NULL
ORDER BY 1;



























-- Importing said Transcripts




SELECT * FROM transcript;


SELECT TOP 1 * FROM transcriptCourse;
SELECT  * FROM PSTranscriptTransfer;


BEGIN TRANSACTION;

INSERT INTO transcriptCourse
	( personID
	, courseNumber
	, stateCode
	, courseName
	, standardNumber
	, standardName
	, districtNumber
	, schoolNumber
	, schoolName
	, status
	, date
	, startYear
	, endYear
	, grade
	, score
	, [percent]
	, gpaWeight
	, gpaValue
	, bonusPoints
	, gpaMax
	, scoreID
	, startTerm
	, endTerm
	, termsLong
	, actualTerm
	, comments
	, unweightedGPAValue
	, coursePart
	, specialEdCode
	, legacyKey
	, districtID
	, modifiedDate
	, modifiedByID
	, courseType
	, repeatCourse
	, collegeCode
	, calendarTerms
	, secondaryCredit
	, transcriptField6
	, transcriptGUID
	, creditRecovery
	, creditInLieu )
SELECT
	  p.personID AS personID
	, pst.courseNum AS courseNumber
	, pst.stateCode AS stateCode
	, pst.courseName AS courseName
	, NULL AS standardNumber
	, NULL AS standardName
	, pst.districtNum AS districtNumber
	, NULL AS schoolNumber
	, pst.schoolName AS schoolName
	, NULL AS status
	, pst.date AS date
	, pst.endYear - 1 AS startYear
	, pst.endYear AS endYear
	, pst.gradeLevel AS grade
	, pst.score AS score
	, NULL AS "percent"
	, pst.gpaWeight AS gpaWeight
	, pst.unweightedGPAValue AS gpaValue
	, NULL AS bonusPoints
	, pst.gpaMax AS gpaMax
	, NULL AS scoreID
	, pst.startTerm AS startTerm
	, pst.endTerm AS endTerm
	, pst.termsLong AS termsLong
	, pst.actualTerm AS actualTerm
	, NULL AS comments
	, pst.unweightedGPAValue AS unweightedGPAValue
	, NULL AS coursePart
	, NULL AS specialEdCode
	, NULL AS legacyKey
	, 26 AS districtID
	, CURRENT_TIMESTAMP AS modifiedDate
	, 525 AS modifiedByID
	, NULL AS courseType
	, 0 AS repeatCourse
	, NULL AS collegeCode
	, NULL AS calendarTerms
	, 0 AS secondaryCredit
	, 0 AS transcriptField6
	, newID() AS transcriptGUID
	, 0 AS creditRecovery
	, 0 AS creditInLieu
FROM PSTranscriptTransfer pst
INNER JOIN person p
ON pst.studentNum = p.studentNumber;

COMMIT;

ROLLBACK TRANSACTION;

SELECT * FROM transcriptCredit;

SELECT * FROM curriculumstandard
WHERE parentID = 44;
SELECT DISTINCT creditTypeName FROM PSTranscriptTransfer;




BEGIN TRANSACTION;

INSERT INTO transcriptCredit
	( personID
	, transcriptID
	, standardID
	, creditsEarned
	, creditsAttempted )
SELECT DISTINCT
	  p.personID
	, t.transcriptID
	, cs.standardID
	, pst.creditsEarned
	, pst.creditsAttempted
FROM transcriptCourse t
LEFT JOIN transcriptCredit tc
ON t.transcriptID = tc.transcriptID
INNER JOIN person p
ON t.personID = p.personID
LEFT JOIN PSTranscriptTransfer pst
ON p.studentNumber = pst.studentNum
AND pst.courseNum = t.courseNumber
AND pst.endYear = t.endYear
AND pst.actualTerm = t.actualTerm
AND pst.startTerm = t.startTerm
AND pst.endTerm = t.endTerm
AND pst.score = t.score
INNER JOIN CurriculumStandard cs
ON pst.creditTypeName = cs.name
AND cs.parentID = 44
WHERE tc.transcriptID IS NULL
AND pst.studentNum IS NOT NULL;

COMMIT;
























