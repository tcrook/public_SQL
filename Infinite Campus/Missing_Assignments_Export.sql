-- ----------------------------------------------------------------------------
-- Author: Tanner Crook
-- Copyright: (c) Tanner Crook 2017
-- ----------------------------------------------------------------------------
-- This script will pull students with missing assignments. The list is ordered 
-- so that it is easily used by homeroom teacher.
-- ----------------------------------------------------------------------------



SELECT
  s.studentNumber AS StudentNumber
, s.lastName AS Last
, s.firstName AS First
, s.grade AS Grade
, c.name AS Course
, sec.teacherDisplay AS CourseTeacher
, lo.name AS MissingAssignmentName
, los.startDate AS assignedDate
, los.endDate AS dueDate
, lpga.totalPoints AS pointsPossible
, vas.homeroomTeacher AS HomeroomTeacher
, vas.teamName AS Team
FROM Person p 
INNER JOIN Student s 
ON p.personID = s.personID
AND s.activeYear = 1
INNER JOIN Roster r 
ON p.personID = r.personID
INNER JOIN Section sec 
ON r.sectionID = sec.sectionID
INNER JOIN Course c 
ON sec.courseID = c.courseID
INNER JOIN Calendar cal 
ON c.calendarID = cal.calendarID
INNER JOIN SectionPlacement sp 
ON sec.sectionID = sp.sectionID
INNER JOIN term t 
ON sp.termID = t.termID
INNER JOIN IMLearningObjectSection los
ON sec.sectionID = los.sectionID
INNER JOIN IMLearningObjectSchedulingSet loss 
ON los.schedulingSetID = loss.schedulingSetID
INNER JOIN IMLearningObject lo 
ON loss.objectID = lo.objectID
INNER JOIN LessonPlanScore lps 
ON los.objectSectionID = lps.objectSectionID
AND sec.sectionID = lps.sectionID
AND p.personID = lps.personID
INNER JOIN LessonPlanGroupActivity lpga 
ON lps.groupActivityID = lpga.groupActivityID
AND los.objectSectionID = lpga.objectSectionID
INNER JOIN V_AdhocStudent vas 
ON p.personID = vas.personID
AND cal.calendarID = vas.calendarID
WHERE cal.name = 'CALENDAR NAME'
AND t.name = 'TERM NAME'
AND ((CURRENT_TIMESTAMP >= r.startDate) OR r.startDate IS NULL) 
AND ((CURRENT_TIMESTAMP <= r.endDate) OR r.endDate IS NULL)
AND lps.missing = 1
ORDER BY s.grade, vas.teamName, vas.homeroomTeacher, s.lastName, s.firstName;