-- =============================================================================
-- future_enrollment.sql
-- (c) Tanner Crook 2018
-- =============================================================================
-- This SQL script will create set the information for students future
-- enrollments. This is found at:
--
-- Index > Student Information > General > Enrollments > (click enrollment)
-- Enrollment.nextCalendar, Enrollment.nextStructureID, Enrollment.nextGrade
--
-- @promotedCalendar is the calendar if the student is moving on to the next
-- school. (ie elementary to middle school)
-- @retainedCalendar is the calendar for the same school but the next year
-- @currentCalendar is the current calendar students are in
-- =============================================================================

BEGIN TRANSACTION;

GO
DECLARE @promotedCalendar VARCHAR(30) = 'NextSchoolCalendar'
, @retainedCalendar VARCHAR(30) = 'NextYearCalendar'
, @currentCalendar VARCHAR(30) = 'CurrentYearCalendar'
, @promotionGrade VARCHAR(3) = 'GradeStudentsMoveToPromotedCalendar'
UPDATE e
SET nextGrade = CASE e.grade
	WHEN 'KG' THEN '01'
	WHEN '01' THEN '02'
	WHEN '02' THEN '03'
	WHEN '03' THEN '04'
	WHEN '04' THEN '05'
	WHEN '05' THEN '06'
	WHEN '06' THEN '07'
	WHEN '07' THEN '08'
	WHEN '08' THEN '09'
	WHEN '09' THEN '10'
	WHEN '10' THEN '11'
	WHEN '11' THEN '12'
	END
, nextCalendar = CASE e.grade
	WHEN @promotionGrade THEN (SELECT calendarID FROM Calendar WHERE name = @promotedCalendar)
	ELSE (SELECT calendarID FROM Calendar WHERE name = @retainedCalendar)
	END
, nextStructureID = CASE e.grade
	WHEN @promotionGrade THEN (SELECT ss.structureID FROM ScheduleStructure ss 
				INNER JOIN Calendar cal ON ss.calendarID = cal.calendarID 
				WHERE cal.name = @promotedCalendar 
				AND ss.name = 'Main')
	ELSE (SELECT ss.structureID FROM ScheduleStructure ss 
		INNER JOIN Calendar cal ON ss.calendarID = cal.calendarID 
		WHERE cal.name = @retainedCalendar 
		AND ss.name = 'Main')
	END
FROM Enrollment e 
INNER JOIN Calendar cal 
ON e.calendarID = cal.calendarID 
WHERE cal.name = @currentCalendar
AND e.grade != '12';
GO

COMMIT;